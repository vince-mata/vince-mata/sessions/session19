console.log("Hello World");

/*
Activity: 1. In the S19 folder, create an activity folder and an index.html
and index.js file inside of it. 2. Link the script.js file to the index.html
file. 
3. Create a variable getCube and use the exponent operator to compute
for the cube of a number. (A cube is any number raised to 3) 

4. Using Template Literals, print out the value of the getCube variable with a message
of The cube of <num> is… 

5. Create a variable address with a value of an
array containing details of an address. 

6. Destructure the array and print
out a message with the full address using Template Literals. 

7. Create a
variable animal with a value of an object data type with different animal
details as it’s properties. 

8. Destructure the object and print out a message
with the details of the animal using Template Literals. 

9. Create an array of
numbers. 

10. Loop through the array using forEach, an arrow function and
using the implicit return statement to print out the numbers. 

11. Create a
class of a Dog and a constructor that will accept a name, age and breed as
it’s properties. 

12. Create/instantiate a new object from the class Dog and
console log the object.
*/




const getCube = 5 ** 3;

let number = 5

message = `The cube of ${number} is ${getCube}`;
console.log(message)

const address = ["256 Washington Ave NW,", "California 90011"]

const [streetName, cityName] = address;

console.log(`I live at ${streetName} ${cityName}`)

const animal = {
	crocodileName: "Lolong",
	crocodileType: "Saltwater Crocodile",
	crocodileWeight: "1075 kgs",
	crocodileMeasurement: "20 ft 3 in"
}

const {crocodileName, crocodileType, crocodileWeight, crocodileMeasurement} = animal;

console.log(`${crocodileName} was a ${crocodileType}. He weighed at ${crocodileWeight} with a measurement of ${crocodileMeasurement}`)




const numbers = ["1", "2", "3", "4", "5"]

numbers.forEach( (number) => {
	console.log(`${number}`)
})

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog()

myDog.name = "Snoopy";
myDog.age = 10;
myDog.breed = "Pitbull";

console.log(myDog)